# Base do IPTU paulistana em formato SQLite

Base copiada ipsis literis de http://dados.prefeitura.sp.gov.br/dataset/base-de-dados-do-imposto-predial-e-territorial-urbano-iptu na tabela iptu_oficial

Há uma outra tabela chamada iptu_oficial_com_campos_uteis que é a mesma tabela, mas acrescida de alguns campos em formatos úteis (tipo campo int) e índices para deixar a busca mais rápida.
